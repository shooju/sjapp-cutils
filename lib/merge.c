
#include "py_defines.h"
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#include <inttypes.h>

static void init_numpy(void)
{
  import_array();
}

typedef struct {
  PyObject *array;
  PyObject *array_changes;
  PyObject *list_deleted;
  PyObject *source;
  char *arrA, *arrB;
  double valMin, valMax, dateMin, dateMax;
  npy_intp len, lenA, lenB;
  npy_intp cnt, cntNew, cntChanged, cntNewChanged, cntDeleted, cntDeleted2;
  int firstVal;
  int sz;
  int type;
} stats_t;

static char *initFlagsAndStats(const PyArrayObject *numpyA, const PyArrayObject *numpyB, stats_t *st)
{
  char *flags;
  int len, idxA, idxB;

  st->source = (PyObject *)numpyA;
  st->lenA = PyArray_DIM(numpyA, 0);
  st->lenB = PyArray_DIM(numpyB, 0);
  st->arrA = (char *)PyArray_GETPTR1((PyArrayObject*)numpyA, 0);
  st->arrB = (char *)PyArray_GETPTR1((PyArrayObject*)numpyB, 0);
  flags = (char*)PyObject_Malloc(st->lenA + st->lenB);
  if (!flags) return NULL;
  st->firstVal = 1;
  for (len = idxA = idxB = 0; idxA < st->lenA || idxB < st->lenB;) {
    int fl = 0;
    if (idxA < st->lenA && idxB < st->lenB) {
      int64_t itA = *(int64_t*)(st->arrA + st->sz * idxA);
      int64_t itB = *(int64_t*)(st->arrB + st->sz * idxB);
      if (itA < itB) {
        fl = 1;
        st->cnt++;
        idxA++;
      } else if (itA > itB) {
        double val = *(double*)(st->arrB + st->sz * idxB + 8);
        if (val == val || st->type == 2) {
          fl = 2;
          st->cnt++;
          st->cntNew++;
        } else fl = 4;
        idxB++;
      } else {
        double valA = *(double*)(st->arrA + st->sz * idxA + 8);
        double valB = *(double*)(st->arrB + st->sz * idxB + 8);
        idxA++;
        idxB++;
        st->cnt++;
        if (valA == valB) {
          fl = 5;
        } else {
          if (valB == valB) {
            fl = 6;
            if (valA == valA) {
                st->cntChanged++;
            } else {
              st->cntNew++;
              st->cntNewChanged++;
            }
          } else {
            if (valA == valA) {
              fl = 10;
              st->cntDeleted++;
            } else {
              fl = 13;
              st->cntDeleted2++;
            }
          }
        }
      }
    } else if (idxA < st->lenA) {
      fl = 1;
      st->cnt++;
      idxA++;
    } else {
      double val = *(double*)(st->arrB + st->sz * idxB + 8);
      if (val == val || st->type == 2) {
        fl = 2;
        st->cnt++;
        st->cntNew++;
      } else fl = 4;
      idxB++;
    }
    flags[len++] = fl;
    if ((fl & 3) && fl < 10) {
      char *arr = ((fl & 1) ? st->arrA : st->arrB);
      int idx = ((fl & 1) ? idxA : idxB) - 1;
      int64_t date = *(int64_t*)(arr + st->sz * idx);
      double val = *(double*)(arr + st->sz * idx + 8);
      if (fl != 1 || val == val) {
        if (st->firstVal) {
          st->dateMin = st->dateMax = date;
          st->valMin = st->valMax = val;
          st->firstVal = 0;
        } else {
          if (st->dateMin > date) st->dateMin = date;
          if (st->dateMax < date) st->dateMax = date;
          if (st->valMin > val) st->valMin = val;
          if (st->valMax < val) st->valMax = val;
        }
      }
    }
  }
  st->len = len;
  return flags;
}

static void fillArrays(stats_t *st, char *flags)
{
  char last = 0;
  npy_intp dims[10] = { 1, 4};
  int cnt = 0, idx = 0, idxA = 0, idxB = 0, idxDel = 0, idxChg = 0, i;
  char *arr;
  PyArray_Descr *dtype = PyArray_DescrFromObject(st->source, NULL);
  dims[0] = st->cnt;
  st->array = PyArray_NewFromDescr(&PyArray_Type, dtype, 1,
                                dims, NULL, NULL,
                                NPY_ARRAY_F_CONTIGUOUS,
                                NULL);

  if (st->type == 1) {
    PyObject *op = Py_BuildValue("[(s, s), (s, s), (s, s), (s, s)]",
      "f0", "i8", "f1", "f8", "f2", "f8", "f3", "i4");
    PyArray_Descr *descr;
    PyArray_DescrConverter(op, &descr);
    Py_DECREF(op);
    dims[0] = st->cntChanged + st->cntDeleted + st->cntNewChanged;
    st->array_changes = PyArray_SimpleNewFromDescr(1, dims, descr);
    st->list_deleted = PyList_New(st->cntDeleted);
  }

  arr = (char *)PyArray_GETPTR1((PyArrayObject*)st->array, 0);
  for (i = 0; i <= st->len; i++) {
    if (i < st->len && (flags[i] & 3) == last) {
      cnt++;
    } else {
      if (last & 1) {
        memcpy(arr + idx * st->sz, st->arrA + (idxA - cnt) * st->sz, cnt * st->sz);
        idx += cnt;
      } else if (last & 2) {
        memcpy(arr + idx * st->sz, st->arrB + (idxB - cnt) * st->sz, cnt * st->sz);
        idx += cnt;
      }
      if (i == st->len) break;
      last = (flags[i] & 3);
      cnt = 1;
    }
    switch (flags[i]) {
      case 1:
        idxA++;
        break;
      case 2:
      case 4:
        idxB++;
        break;
      case 10:
        if (st->type == 1) PyList_SetItem(st->list_deleted, idxDel++, PyLong_FromLongLong(*(int64_t*)(st->arrB + st->sz * idxB)));
        // walkthrough
      case 6:
        if (st->type == 1) {
          char *ptr = (char *)PyArray_GETPTR2((PyArrayObject*)st->array_changes, idxChg++, 0);
          *(int64_t*)(ptr) = *(int64_t *)(st->arrB + idxB * st->sz);
          *(double*)(ptr + 8) = *(double *)(st->arrA + idxA * st->sz + 8);
          *(double*)(ptr + 16) = *(double *)(st->arrB + idxB * st->sz + 8);
          *(int32_t*)(ptr + 24) = *(int32_t *)(st->arrA + idxA * st->sz + 16);
        }
        // walkthrough
      case 13:
      case 5:
        idxA++;
        idxB++;
        break;
    }
  }

  PyObject_Free(flags);
}

static PyObject *createResult(stats_t *st)
{
  PyObject *res;
  PyObject *stats;
  PyObject *val;

  res = PyTuple_New(3);
  stats = PyDict_New();

  val = PyLong_FromLongLong(st->lenB - st->cntDeleted2);
  PyDict_SetItemString(stats, "processed_points", val);
  Py_DECREF(val);
  val = PyLong_FromLongLong(st->cntChanged);
  PyDict_SetItemString(stats, "changed_points", val);
  Py_DECREF(val);
  val = PyLong_FromLongLong(st->cntNew);
  PyDict_SetItemString(stats, "new_points", val);
  Py_DECREF(val);
  val = PyLong_FromLongLong(st->cntDeleted);
  PyDict_SetItemString(stats, "deleted_points", val);
  Py_DECREF(val);
  PyDict_SetItemString(stats, "deleted_dates", st->list_deleted);
  Py_DECREF(st->list_deleted);
  val = st->firstVal ? Py_None : PyLong_FromLongLong((int64_t)st->dateMin);
  PyDict_SetItemString(stats, "dates_min", val);
  Py_DECREF(val);
  val = st->firstVal ? Py_None : PyLong_FromLongLong((int64_t)st->dateMax);
  PyDict_SetItemString(stats, "dates_max", val);
  Py_DECREF(val);
  val = st->firstVal ? Py_None : PyFloat_FromDouble(st->valMin);
  PyDict_SetItemString(stats, "min", val);
  Py_DECREF(val);
  val = st->firstVal ? Py_None : PyFloat_FromDouble(st->valMax);
  PyDict_SetItemString(stats, "max", val);
  Py_DECREF(val);

  PyTuple_SetItem(res, 0, st->array);
  PyTuple_SetItem(res, 1, stats);
  PyTuple_SetItem(res, 2, st->array_changes);

  return res;
}

PyObject* MergePoints(PyObject* self, PyObject *args, PyObject *kwargs, int type)
{
  const PyArrayObject *numpyA;
  const PyArrayObject *numpyB;
  PyObject *incJI = NULL;
  PyObject *incTS = NULL;
  char *flags;
  stats_t st;
  int include_jobs, include_ts;
  if (type == 1) {
    char *kwlist[] = {"numpy_a", "numpy_b", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", kwlist, &numpyA, &numpyB)) {
      return NULL;
    }
    include_jobs = 1;
    include_ts = 1;
  } else {
    char *kwlist[] = {"numpy_a", "numpy_b", "include_jobs", "include_timestamps", NULL};
    include_jobs = 0;
    include_ts = 0;
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|OO", kwlist, &numpyA, &numpyB, &incJI, &incTS)) {
      return NULL;
    }
    if (incJI != NULL && PyObject_IsTrue(incJI)) include_jobs = 1;
    if (incTS != NULL && PyObject_IsTrue(incTS)) include_ts = 1;
  }

  init_numpy();

  memset(&st, 0, sizeof(stats_t));
  st.type = type;
  st.sz = 16 + (include_jobs ? 4 : 0) + (include_ts ? 8 : 0);

  if (!PyArray_Check(numpyA) || !PyArray_Check(numpyB)) {
    PyErr_Format(PyExc_TypeError, "Expected two numpy arrays");
    return NULL;
  }

  flags = initFlagsAndStats(numpyA, numpyB, &st);
  if (!flags) return PyErr_NoMemory();
  fillArrays(&st, flags);

  if (type == 2) return st.array;

  return createResult(&st);
}

PyObject* MergePoints1(PyObject* self, PyObject *args, PyObject *kwargs)
{
  return MergePoints(self, args, kwargs, 1);
}

PyObject* MergePoints2(PyObject* self, PyObject *args, PyObject *kwargs)
{
  return MergePoints(self, args, kwargs, 2);
}
