
#include "py_defines.h"
#include "version.h"


PyObject* MergePoints1(PyObject* self, PyObject *args, PyObject *kwargs);
PyObject* MergePoints2(PyObject* self, PyObject *args, PyObject *kwargs);
PyObject* CalculateFrequency(PyObject* self, PyObject *args, PyObject *kwargs);

static PyMethodDef cutilsMethods[] = {
  {"merge_point_arrays", (PyCFunction) MergePoints1, METH_VARARGS | METH_KEYWORDS, "Merges 2 points array into 1 by key column with stats"},
  {"merge_point_arrays_light", (PyCFunction) MergePoints2, METH_VARARGS | METH_KEYWORDS, "Merges 2 points array into 1 by key column"},
  {"calculate_frequency", (PyCFunction) CalculateFrequency, METH_VARARGS | METH_KEYWORDS, "calculate frequency of milli dates array"},
  {NULL, NULL, 0, NULL}
};

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  "sjapp_cutils",
  0,              /* m_doc */
  -1,             /* m_size */
  cutilsMethods,  /* m_methods */
  NULL,           /* m_reload */
  NULL,           /* m_traverse */
  NULL,           /* m_clear */
  NULL            /* m_free */
};

#define PYMODINITFUNC       PyObject *PyInit_sjapp_cutils(void)
#define PYMODULE_CREATE()   PyModule_Create(&moduledef)
#define MODINITERROR        return NULL

#else

#define PYMODINITFUNC       PyMODINIT_FUNC initsjapp_cutils(void)
#define PYMODULE_CREATE()   Py_InitModule("sjapp_cutils", cutilsMethods)
#define MODINITERROR        return

#endif

PYMODINITFUNC
{
  PyObject *module;
  PyObject *version_string;

  module = PYMODULE_CREATE();

  if (module == NULL)
  {
    MODINITERROR;
  }

  version_string = PyString_FromString (CUTILS_VERSION);
  PyModule_AddObject (module, "__version__", version_string);
#if PY_MAJOR_VERSION >= 3
  return module;
#endif
}
