
#include "py_defines.h"
#include <inttypes.h>

static int isLeap(int year)
{
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);;
}

static int getExtraDays(int year)
{
	return (year / 4) - (year / 100) + (year / 400);
}

const int64_t epoch1970 = 62167132800;
static int month_days[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
static int month_leap_days[] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 , 366};

typedef struct {
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    int dow;
} dt_t;

void milli_to_time(dt_t *dt, int64_t value)
{
	value /= 1000;
	value += epoch1970;
	dt->dow = (value / 86400 + 6) % 7;
	int sec = value % 60;
	value /= 60;
	int min = value % 60;
	value /= 60;
	int hour = value % 24;
	value /= 24;
	int year = (int)value / 365;
	int days = (int)value % 365;
	for (;;) {
		int extra_days = getExtraDays(year - 1);
		if (extra_days > days) {
			days += 365;
			year--;
		} else {
			days -= extra_days;
			break;
		}
	}
	int month = 0;
	int *md = (isLeap(year) ? month_leap_days : month_days);
	while (days >= md[month + 1]) month++;
	days -= md[month];

	dt->year = year;
    dt->month = month + 1;
    dt->day = days + 1;
    dt->hour = hour;
    dt->minute = min;
    dt->second = sec;
}

PyObject* CalculateFrequency(PyObject* self, PyObject *args, PyObject *kwargs, int type)
{
    char *kwlist[] = {"milli_array", NULL};
    PyObject *arr;
    int is_month_first = 1; // all dates have YYYY-01-DD
    int is_day_first = 1;   // all dates have YYYY-MM-01
    int is_hour_zero = 1;   // all dates have T00:00
    int is_weekly = 1;      // all dates are in the first day of the week (monday)
    int is_quarterly = 1;   // all dates fall in the months 1, 4, 7 and 10
    int is_minute_zero = 1;
    size_t len;
  
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", kwlist, &arr)) {
        return NULL;
    }
  
    if (!PyList_Check(arr)) {
        PyErr_Format(PyExc_TypeError, "Expected array of millis");
        return NULL;
    }
  
    len = PyList_Size(arr);
    if (len == 0) {
    	Py_RETURN_NONE;
    }
    size_t i;
  
    for (i = 0; i < len; i++) {
        uint64_t date = PyLong_AsLongLong(PyList_GetItem(arr, i));
        dt_t dt;
        milli_to_time(&dt, date);
        if (dt.second != 0) return PyString_FromString("x");
        is_month_first = is_month_first && dt.month == 1;
        is_day_first = is_day_first && dt.day == 1;
        is_hour_zero = is_hour_zero && dt.hour == 0;
        is_minute_zero = is_minute_zero && dt.minute == 0;
        is_weekly = is_weekly && dt.dow == 0;
        is_quarterly = is_quarterly && (dt.month == 1 || dt.month == 4 || dt.month == 7 || dt.month == 10);
    }
    // at this point all dates have 0 seconds and 0 microseconds
    if (!is_minute_zero) return PyString_FromString("m");
    else if (!is_hour_zero) return PyString_FromString("h");
  
    // at this point all dates have T00:00
    // checking if all days are 1 to narrow down frequency faster
    if (is_day_first) {
        // at this all dates have YYYY-MM-01 T00:00
        // checking by order of precedence
        if (is_month_first) { // yearly has precedence
            // at this all dates have YYYY-01-01 T00:00
            return PyString_FromString("y");
        }
        if (is_quarterly) {
            return PyString_FromString("q");
        } else return PyString_FromString("M");
    }
    if (is_weekly) return PyString_FromString("w"); // weekly has precedence
    // at this point we exhausted all other options, date is in the form YYYY-MM-DD T00:00
    return PyString_FromString("d");
}
