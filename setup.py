from distutils.core import setup, Extension
import numpy as np
import re
import os

extmod = Extension('sjapp_cutils',
                   sources = ['./lib/cutils.c', './lib/merge.c', './lib/calculate_frequency.c'],
                   include_dirs=[np.get_include()])

def get_version():
    filename = os.path.join(os.path.dirname(__file__), './lib/version.h')
    file = None
    try:
        file = open(filename)
        header = file.read()
    finally:
        if file:
            file.close()
    m = re.search(r'#define\s+CUTILS_VERSION\s+"(\d+\.\d+(?:\.\d+)?)"', header)
    assert m, "version.h must contain CUTILS_VERSION macro"
    return m.group(1)


setup(name='sjapp-cutils',
      version=get_version(),
      description='C functions for sjapp.',
      install_requires=[
          'numpy',
      ],
      ext_modules=[extmod])
