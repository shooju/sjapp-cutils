from unittest import TestCase
from datetime import tzinfo, timedelta, datetime
from sjapp_cutils import calculate_frequency as calculate_frequency_c

FIRST_DAY_OF_WEEK = 1

def calculate_frequency(millis):
    if not len(millis):
        return None

    is_month_first = True  # all dates have YYYY-01-DD
    is_day_first = True  # all dates have YYYY-MM-01
    is_hour_zero = True  # all dates have T00:00
    is_weekly = True  # all dates are in the first day of the week (monday)
    is_quarterly = True  # all dates fall in the months 1, 4, 7 and 10
    is_minute_zero = True
    for date in millis:
        dt = datetime.utcfromtimestamp(date / 1000)
        if dt.second != 0 or dt.microsecond != 0:
            # short circuiting, it can't be any of the others
            return 'x'

        is_month_first = is_month_first and dt.month == 1
        is_day_first = is_day_first and dt.day == 1
        is_hour_zero = is_hour_zero and dt.hour == 0
        is_minute_zero = is_minute_zero and dt.minute == 0

        is_weekly = is_weekly and dt.isoweekday() == FIRST_DAY_OF_WEEK
        is_quarterly = is_quarterly and dt.month in (1, 4, 7, 10)

    # at this point all dates have 0 seconds and 0 microseconds
    if not is_minute_zero:
        return 'm'
    elif not is_hour_zero:
        return 'h'

    # at this point all dates have T00:00

    # checking if all days are 1 to narrow down frequency faster
    if is_day_first:
        # at this all dates have YYYY-MM-01 T00:00
        # checking by order of precedence
        if is_month_first:  # yearly has precedence
            # at this all dates have YYYY-01-01 T00:00
            return 'y'
        if is_quarterly:
            return 'q'
        else:
            return 'M'
    if is_weekly:  # weekly has precedence
        return 'w'
    # at this point we exhausted all other options, date is in the form YYYY-MM-DD T00:00
    return 'd'

class TestCalculateFrequency(TestCase):
    def test_empty_array(self):
        arr = []
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_not_zero_seconds(self):
        arr = [1000, 2000]
        self.assertEqual(calculate_frequency(arr), 'x')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_minute(self):
        arr = [1504862340000]
        self.assertEqual(calculate_frequency(arr), 'm')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_hour(self):
        arr = [1504861200000]
        self.assertEqual(calculate_frequency(arr), 'h')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_day(self):
        arr = [1504828800000, 242524800000]
        self.assertEqual(calculate_frequency(arr), 'd')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_month(self):
        arr = [1485907200000, 241920000000]
        self.assertEqual(calculate_frequency(arr), 'M')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_quarter(self):
        arr = [1483228800000, 1491004800000, 1498867200000, 1506816000000]
        self.assertEqual(calculate_frequency(arr), 'q')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_year(self):
        arr = [1483228800000, 220924800000]
        self.assertEqual(calculate_frequency(arr), 'y')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
    def test_weekly(self):
        arr = [155174400000, 1504483200000]
        self.assertEqual(calculate_frequency(arr), 'w')
        self.assertEqual(calculate_frequency(arr), calculate_frequency_c(arr))
