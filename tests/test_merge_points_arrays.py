from unittest import TestCase
from sjapp_cutils import merge_point_arrays, merge_point_arrays_light
from numpy import nan, array, ndarray, dtype


class TestMergeArrays(TestCase):
    def test_merge_arrays_light(self):
        dt = dtype('i8,f8')
        res = merge_point_arrays_light(
            array([(0, 1.0)], dt),
            array([(0, 10),
                   (1, nan),
                   (2, 10)], dt)
        )
        self.assert_array(res,
                          [[0, 10],
                           [1, nan],
                           [2, 10]])
        dt = dtype('i8,f8,i4')

        res = merge_point_arrays_light(
            array([(0, 1.0, 1)], dt),
            array([(0, 10, 2),
                   (1, nan, 3),
                   (2, 10, 4)], dt),
            include_jobs=True
        )

        self.assert_array(res,
                          [[0, 10, 2],
                           [1, nan, 3],
                           [2, 10, 4]], )

        dt = dtype('i8,f8,i4,u8')

        res = merge_point_arrays_light(
            array([(0, 1.0, 1, 100)], dt),
            array([(0, 10, 2, 200),
                   (1, nan, 3, 300),
                   (2, 10, 4, 400)], dt),
            include_jobs=True, include_timestamps=True
        )

        self.assert_array(res,
                          [[0, 10, 2, 200],
                           [1, nan, 3, 300],
                           [2, 10, 4, 400]], )

    def test_merge_arrays_light_with_multiple_fields(self):
        dt = [('dates', 'i8'),
              ('values', 'f8'),
              ('jobs', 'i4'),
              ('ts', 'u8')]

        array_a = array([(0, 1.0, 10, 11)], dt)
        array_b = array([(0, 10),
                         (1, nan),
                         (2, 10)], [('dates', 'i8'), ('values', 'f8')])

        res = merge_point_arrays_light(array_a[['dates', 'values']], array_b,
         include_jobs=False, include_timestamps=False)
        self.assert_array(res,
                          [[0, 10],
                           [1, nan],
                           [2, 10]])
                           
    def test_merge_arrays(self):
        # create fully new array
        res, stats, changes = self._merge(
            [],
            [[0, 10],
             [1, nan],
             [2, 10]],
        )
        self.assert_array(res,
                         [[0, 10, 2, 2],
                          [2, 10, 2, 2]], )

        self.assertEqual(stats, {'new_points': 2,
                                 'deleted_points': 0,
                                 'min': 10.0,
                                 'max': 10.0,
                                 'changed_points': 0,
                                 'dates_min': 0,
                                 'dates_max': 2,
                                 'deleted_dates': [],
                                 'processed_points': 3})

        self.assert_array(changes, [])

        # write over deleted points
        res, stats, changes = self._merge(
            [[0, nan],
             [1, nan],
             [2, nan],
             [3, nan]],
            [[0, 10],
             [1, nan],
             [2, 10]],
        )
        self.assert_array(res,
                         [[0.0, 10.0, 2.0, 2.0],
                          [1.0, nan, 1.0, 1.0],
                          [2.0, 10.0, 2.0, 2.0],
                          [3.0, nan, 1.0, 1.0]], )

        self.assertEqual(stats, {'new_points': 2,
                                 'deleted_points': 0,
                                 'min': 10.0, 'max': 10.0,
                                 'changed_points': 0,
                                 'dates_min': 0, 'dates_max': 2,
                                 'deleted_dates': [],
                                 'processed_points': 2})

        self.assert_array(changes, [[0.0, nan, 10.0, 1.0],
                                    [2.0, nan, 10.0, 1.0]])

        # delete completely un-existing points
        res, stats, changes = self._merge(
            [],
            [[0, nan],
             [1, nan],
             [2, nan],
             [2, nan]],
        )
        self.assert_array(res, [],)

        self.assertEqual(stats, {'new_points': 0,
                                 'deleted_points': 0,
                                 'min': None, 'max': None,
                                 'changed_points': 0,
                                 'dates_min': None, 'dates_max': None,
                                 'deleted_dates': [],
                                 'processed_points': 4})

        self.assert_array(changes, [])

        # misc cases
        res, stats, changes = self._merge(
            [[0, 1],
             [1, 2],
             [2, nan],
             [3, nan],
             [4, 5]],
            [[0, 10],
             [1, nan],
             [2, 10],
             [3, nan],
             [6, 10],
             [7, 12]],
        )

        self.assert_array(res,
                          [[0.0, 10.0, 2.0, 2.0],
                           [1.0, nan, 2.0, 2.0],
                           [2.0, 10.0, 2.0, 2.0],
                           [3.0, nan, 1.0, 1.0],
                           [4.0, 5.0, 1.0, 1.0],
                           [6.0, 10.0, 2.0, 2.0],
                           [7.0, 12.0, 2.0, 2.0]])
        self.assertEqual(stats, {'new_points': 3,
                                 'deleted_points': 1,
                                 'min': 5.0,
                                 'max': 12.0,
                                 'changed_points': 1,
                                 'dates_min': 0,
                                 'dates_max': 7,
                                 'deleted_dates': [1],
                                 'processed_points': 5})
        self.assert_array(changes, [[0.0, 1.0, 10.0, 1.0],
                                   [1.0, 2.0, nan, 1.0],
                                   [2.0, nan, 10.0, 1.0]])

        res, stats, changes = self._merge(
            [[0, -4.0, ],
             [1, 15.0, ],
             [2, nan, ],
             [3, 18.0, ],
             [4, 10.0, ]],

            [[0, nan, ],
             [3, nan, ],
             [4, nan, ]],
        )

        self.assert_array(res, [[0.0, nan, 2.0, 2.0],
                                [1.0, 15.0, 1.0, 1.0],
                                [2.0, nan, 1.0, 1.0],
                                [3.0, nan, 2.0, 2.0],
                                [4.0, nan, 2.0, 2.0]])

        self.assertEqual(stats, {'new_points': 0,
                                 'deleted_points': 3,
                                 'min': 15.0,
                                 'max': 15.0,
                                 'changed_points': 0,
                                 'dates_min': 1,
                                 'dates_max': 1,
                                 'deleted_dates': [0, 3, 4],
                                 'processed_points': 3})

        res, stats, changes = self._merge(
            [[1, 11],
             [2, nan],
             [3, nan],
             [9, nan]],
            [[0, 10],
             [1, nan],
             [2, 20],
             [5, 10]],
        )

        self.assert_array(res, [[0.0, 10.0, 2.0, 2.0],
                               [1.0, nan, 2.0, 2.0],
                               [2.0, 20.0, 2.0, 2.0],
                               [3.0, nan, 1.0, 1.0],
                               [5.0, 10.0, 2.0, 2.0],
                               [9.0, nan, 1.0, 1.0]])

        self.assertEqual(stats, {'new_points': 3,
                                 'deleted_points': 1,
                                 'min': 10.0, 'max': 20.0,
                                 'changed_points': 0,
                                 'dates_min': 0,
                                 'dates_max': 5,
                                 'deleted_dates': [1],
                                 'processed_points': 4})

        self.assert_array(changes, [[1.0, 11.0, nan, 1.0],
                                    [2.0, nan, 20.0, 1.0]])

        res, stats, changes = merge_point_arrays(
            array([(1262304000000, 25., 1, 1513264808566)], dtype('i8,f8,i4,u8')),
            array([(1262304000000, nan, 1, 1513264810706),
                   (1293840000000, 27., 1, 1513264810706)], dtype('i8,f8,i4,u8'))
        )
        self.assert_array(changes, [[1262304000000, 25.0, nan, 1]])
        self.assert_array(res, [[1262304000000, nan, 1, 1513264810706],
                               [1293840000000, 27.0, 1, 1513264810706]])

        self.assertEqual(stats, {'new_points': 1,
                                 'deleted_points': 1,
                                 'min': 27.0,
                                 'max': 27.0,
                                 'changed_points': 0,
                                 'dates_min': 1293840000000,
                                 'dates_max': 1293840000000,
                                 'deleted_dates': [1262304000000],
                                 'processed_points': 2})

    def assert_array(self, a, b):
        if isinstance(a, ndarray):
            a = a.tolist()
        if isinstance(b, ndarray):
            b = b.tolist()
        self.assertEqual([[i if i == i else None for i in e] for e in a],
                         [[i if i == i else None for i in e] for e in b])

    def _merge(self, a, b):
        a = list(map(tuple, [i + [1, 1] for i in a]))
        b = list(map(tuple, [i + [2, 2] for i in b]))
        dt = dtype('i8,f8,i4,u8')
        return merge_point_arrays(array(a, dt),
                                  array(b, dt))
