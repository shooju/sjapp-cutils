import sjapp_cutils as sj
import numpy as np

arrA = np.array([[1, 2, 1, 2], [4, np.nan, 7, 8]], dtype = np.double)
arrB = np.array([[2, 4, 3, 4], [3, np.nan, 5, 6], [4, np.nan, 9, 10]], dtype = np.double)
r = sj.merge_point_arrays(arrA, arrB)
print(r)
print(sj.merge_point_arrays(np.array([[4, np.nan, 7, 8]]), np.array([[4, np.nan, 7, 8]])))

a = np.array([[1293840000000.0, -4.0, 2.0, 1495817627973.0],  [1325376000000.0, 15.0, 2.0, 1495817627973.0],  [1356998400000.0, np.nan, 3.0, 1495817628450.0],  [1388534400000.0, 18.0, 2.0, 1495817627973.0],  [1420070400000.0, 10.0, 2.0, 1495817627973.0]], dtype = np.double)
b = np.array([[1293840000000.0, np.nan, 4.0, 1495817636305.0], [1388534400000.0, np.nan, 4.0, 1495817636305.0], [1420070400000.0, np.nan, 4.0, 1495817636305.0]], dtype = np.double)
print(sj.merge_point_arrays(a, b))
